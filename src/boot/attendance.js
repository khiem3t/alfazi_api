/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-17 09:50:02
*------------------------------------------------------- */

import { CronJob } from 'cron';
import eachLimit from 'async/eachLimit';

export default (app) => {
	new CronJob({ // eslint-disable-line
		cronTime: '00 00 04 * * *',
		onTick: function () {
			/*
			* Runs every day at 04:00:00 AM.
			*/
			console.log('users attendance ------- start at ', new Date());
			const User = app.models.user;

			User.find({}, (err, users) => {
				if (err) {
					console.log('Cronjob attendance error:', err);
					return err;
				}

				let index = 0;

				eachLimit(users, 10, function (user, callback) {

					// Perform operation on file here.
					console.log('Processing user ' + index + ' ---' + user.email);
					let { pointsCount = 0 } = user;

					index++;

					user.updateAttributes({
						attendance: {
							loggedIn: false,
							oldPoints: pointsCount,
						},
					}, (errr) => {
						if (errr) {
							console.log('A user failed to process ---' + user.email, errr);
						}
						callback();
					});
				}, (er) => {
					// if any of the user processing produced an error, err would equal that error
					if (er) {
						// One of the iterations produced an error.
						// All processing will now stop.
						console.log('A user failed to process', er);
					} else {
						console.log('All users have been processed successfully at ', new Date());
					}
				});
			});
		},
		start: true,
		timeZone: 'Asia/Ho_Chi_Minh',
	});
};

