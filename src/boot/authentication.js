/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-12 00:53:26
*------------------------------------------------------- */

export default function enableAuthentication(server) {
	// enable authentication
	server.enableAuth();
}
