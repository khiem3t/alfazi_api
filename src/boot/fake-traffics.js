/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-05-12 16:26:12
*------------------------------------------------------- */
import moment from 'moment';
import { CronJob } from 'cron';

const getRandomIntInclusive = (min, max) => {
	const minNum = Math.ceil(min);
	const maxNum = Math.floor(max);

	return Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum; // The maximum is inclusive and the minimum is inclusive
};

export default (app) => {
	new CronJob({ // eslint-disable-line
		cronTime: '0 */15 * * * *',
		onTick: function () {
			/*
			* Runs every day at 04:00:00 AM.
			*/
			console.log('Fake traffics ------- start at ', new Date());

			const Question = app.models.Question;
			const Notification = app.models.Notification;

			const skip = getRandomIntInclusive(0, 222);
			const limit = getRandomIntInclusive(1, 3);

			Question.find({
				skip,
				limit,
				where: {
					isExampleData: true,
					status: 'active',
					// createdAt: '', // nếu có thể sẽ lấy câu hỏi của trước đó 3 ngày
				},
				include: 'answers',
			}).then((questions) => {
				// update câu hỏi
				questions.forEach((ques) => {
					ques.updateAttributes({
						createdAt: new Date(),
						updatedAt: new Date(),
					}).then(() => {
						const answerList = ques.answers();

						// update câu trả lời
						answerList.forEach((answer, i) => {
							answer.updateAttributes({
								createdAt: moment(new Date()).add(10 + (i * 2), 's').toDate(),
								updatedAt: moment(new Date()).add(10 + (i * 2), 's').toDate(),
							});
						});

						// báo có câu trả lừi mới
						Notification.create({
							type: 'questionCreated',
							creatorId: ques.creatorId,
							questionId: ques.id,
							data: {
								question: ques.__data,
							},
						});
						console.log('Fake traffics done at ', new Date());
					});
				});
			}).catch((err) => {
				console.log('err', err);
			});
		},
		start: true,
		timeZone: 'Asia/Ho_Chi_Minh',
	});
};
