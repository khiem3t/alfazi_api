import { CronJob } from 'cron';

export default (app) => {
	new CronJob({ // eslint-disable-line
		cronTime: '0 4 * * *',
		// cronTime: '* * * * *',
		onTick: function () {
			/*
			* Runs every day at 04:00:00 AM.
			*/
			console.log('Cronjob rankDate ------- start at ', new Date());

			const TransactionPoint = app.models.TransactionPoint;
			const AchievementUnlock = app.models.AchievementUnlock;
			const BadgeUnlock = app.models.BadgeUnlock;
			const Ranking = app.models.Ranking;

			TransactionPoint.getDataSource().connector.connect((err, db) => {
				if (err) {
					console.log('Cronjob rankDate error:', err);
					return err;
				}
				const collection = db.collection('TransactionPoint');
				let beforeDay = new Date(new Date().setDate(new Date().getDate()-1));
				let day = beforeDay.getDate();
				let month = beforeDay.getMonth() + 1;
				let year = beforeDay.getFullYear();

				const option = [

					{
						$project:
						{
							amount: '$amount',
							createdAt: '$createdAt',
							creatorId: '$creatorId',
							year: { $year: '$createdAt' },
							month: { $month: '$createdAt' },
							day: { $dayOfMonth: '$createdAt' },
							week: { $week: '$createdAt' }
						}
					},
					{ $match: {
						day: day,
						month: month,
						year: year,
					} },
					{
						$group:
						{
							_id: '$creatorId',
							creatorId: { $first: '$creatorId' },
							createdAt: { $first: '$createdAt' },
							year: { $first: '$year' },
							month: { $first: '$month' },
							day: { $first: '$day' },
							week: { $first: '$week' },
							pointsCount: { $sum: '$amount' },
						},
					},
					{ $sort: { pointsCount: -1 } },
					{ $limit: 10 }

				];

				collection.aggregate(option).toArray((errr, data) => {
					if (errr) {
						console.log('Cronjob rankDate error:', errr);
						return errr;
					}

					if (data.length > 0) {
						const outPut = data.map((x, i) => {
							return {
								type: 'rankDate',
								creatorId: x.creatorId,
								pointsCount: x.pointsCount,
								month: x.month,
								year: x.year,
								day: x.day,
								week: x.week,
								top: i + 1
							};
						});

						Ranking.create(outPut, (e, rankings) => {
							if (e) {
								console.log('Cronjob rankDate error:', e);
								return e;
							}

							rankings.length = 3;
							rankings.forEach((ranking) => {
								if (ranking && ranking.top === 1) {
									AchievementUnlock.create({
										achievementId: 'reachedTop1Day',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});

									BadgeUnlock.findOrCreate({
										where: {
											badgeId: 'reachedTop1Day',
											userId: ranking.creatorId,
										}
									}, {
										badgeId: 'reachedTop1Day',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 2) {
									AchievementUnlock.create({
										achievementId: 'reachedTop2Day',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 3) {
									AchievementUnlock.create({
										achievementId: 'reachedTop3Day',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
							});
							console.log('Cronjob rankDate ------- end at ', new Date());
						});
					}
				});
			});

		},
		start: true,
		timeZone: 'Asia/Ho_Chi_Minh',
	});
};
