import { CronJob } from 'cron';

export default (app) => {
	new CronJob({ // eslint-disable-line
		cronTime: '0 4 1 * *',
		// cronTime: '* * * * *',
		onTick: function () {
			console.log('Cronjob rankingMonth ------- start at ', new Date());

			const TransactionPoint = app.models.TransactionPoint;
			const AchievementUnlock = app.models.AchievementUnlock;
			const BadgeUnlock = app.models.BadgeUnlock;
			const Ranking = app.models.Ranking;


			TransactionPoint.getDataSource().connector.connect((err, db) => {
				if (err) {
					console.log('Cronjob ranking month error:', err);
					return err;
				}
				const collection = db.collection('TransactionPoint');
				// const beforeMonth = new Date(new Date().setMonth(new Date().getMonth()));
				const now = new Date();
				let month = now.getMonth();
				let year = now.getFullYear();

				if (month === 0){
					month = 12;
					year = year - 1;
				}

				const option = [

					{
						$project:
						{
							amount: '$amount',
							createdAt: '$createdAt',
							creatorId: '$creatorId',
							year: { $year: '$createdAt' },
							month: { $month: '$createdAt' },
							day: { $dayOfMonth: '$createdAt' },
							week: { $week: '$createdAt' }
						}
					},
					{ $match: {
						month: month,
						year: year
					}
					},
					{
						$group:
						{
							_id: '$creatorId',
							creatorId: { $first: '$creatorId' },
							createdAt: { $first: '$createdAt' },
							year: { $first: '$year' },
							month: { $first: '$month' },
							day: { $first: '$day' },
							week: { $first: '$week' },
							pointsCount: { $sum: '$amount' },
						},
					},
					{ $sort: { pointsCount: -1 } },
					{ $limit: 10 }

				];

				collection.aggregate(option).toArray((errr, data) => {
					if (errr) {
						console.log('Cronjob ranking month error:', errr);
						return errr;
					}

					if (data.length > 0) {
						const outPut = data.map((x, i) => {
							return {
								type: 'rankMonth',
								creatorId: x.creatorId,
								pointsCount: x.pointsCount,
								month: x.month,
								year: x.year,
								top: i + 1
							};
						});

						Ranking.create(outPut, (e, rankings) => {
							if (e) {
								console.log('Cronjob ranking month error:', e);
								return e;
							}

							rankings.length = 3;
							rankings.forEach((ranking) => {
								if (ranking && ranking.top === 1) {
									AchievementUnlock.create({
										achievementId: 'reachedTop1Month',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});

									BadgeUnlock.findOrCreate({
										where: {
											badgeId: 'reachedTop1Month',
											userId: ranking.creatorId,
										}
									}, {
										badgeId: 'reachedTop1Month',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 2) {
									AchievementUnlock.create({
										achievementId: 'reachedTop2Month',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 3) {
									AchievementUnlock.create({
										achievementId: 'reachedTop3Month',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
							});
							console.log('Cronjob rankingMonth ------- end at ', new Date());
						});
					}
				});
			});
		},
		start: true,
		timeZone: 'Asia/Ho_Chi_Minh',
	});
};
