import { CronJob } from 'cron';
import weekOfYear from 'src/utils/week-of-year';

export default (app) => {
	new CronJob({ // eslint-disable-line
		cronTime: '0 4 * * 1',
		// cronTime: '* * * * *',
		onTick: function () {
			console.log('Cronjob rankingWeek ------- start at ', new Date());

			const TransactionPoint = app.models.TransactionPoint;
			const AchievementUnlock = app.models.AchievementUnlock;
			const BadgeUnlock = app.models.BadgeUnlock;
			const Ranking = app.models.Ranking;
			const now = new Date();

			TransactionPoint.getDataSource().connector.connect((err, db) => {
				if (err) {
					console.log('Cronjob ranking week error:', err);
					return err;
				}
				const collection = db.collection('TransactionPoint');
				let year = now.getFullYear();
				let week = weekOfYear(now) - 1;

				if (weekOfYear(now) === 1) {
					let beforeYear = new Date(new Date().setFullYear(new Date().getFullYear()-1));

					year = beforeYear.getFullYear();
					week = 53;
				}

				const option = [

					{
						$project:
						{
							amount: '$amount',
							createdAt: '$createdAt',
							creatorId: '$creatorId',
							year: { $year: '$createdAt' },
							month: { $month: '$createdAt' },
							day: { $dayOfMonth: '$createdAt' },
							week: { $week: '$createdAt' }
						}
					},
					{ $match: { week: week, year: year } },
					{
						$group:
						{
							_id: '$creatorId',
							creatorId: { $first: '$creatorId' },
							createdAt: { $first: '$createdAt' },
							year: { $first: '$year' },
							month: { $first: '$month' },
							day: { $first: '$day' },
							week: { $first: '$week' },
							pointsCount: { $sum: '$amount' },
						},
					},
					{ $sort: { pointsCount: -1 } },
					{ $limit: 10 }

				];

				collection.aggregate(option).toArray((errr, data) => {
					if (errr) {
						console.log('Cronjob ranking week error:', errr);
						return errr;
					}

					if (data.length > 0) {
						const outPut = data.map((x, i) => {
							return {
								type: 'rankWeek',
								creatorId: x.creatorId,
								pointsCount: x.pointsCount,
								year: x.year,
								week: x.week,
								top: i + 1
							};
						});

						Ranking.create(outPut, (e, rankings) => {
							if (e) {
								console.log('Cronjob ranking week error:', e);
								return e;
							}

							rankings.length = 3;
							rankings.forEach((ranking) => {
								if (ranking && ranking.top === 1) {
									AchievementUnlock.create({
										achievementId: 'reachedTop1Week',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});

									BadgeUnlock.findOrCreate({
										where: {
											badgeId: 'reachedTop1Week',
											userId: ranking.creatorId,
										}
									}, {
										badgeId: 'reachedTop1Week',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 2) {
									AchievementUnlock.create({
										achievementId: 'reachedTop2Week',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
								if (ranking && ranking.top === 3) {
									AchievementUnlock.create({
										achievementId: 'reachedTop3Week',
										userId: ranking.creatorId,
										rakingId: ranking.id,
									});
								}
							});
							console.log('Cronjob rankingWeek ------- end at ', new Date());
						});
					}
				});
			});
		},
		start: true,
		timeZone: 'Asia/Ho_Chi_Minh',
	});
};
