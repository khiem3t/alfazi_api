/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2017-12-01 15:58:41
*------------------------------------------------------- */

export default function(server) {
	// Install a `/` route that returns server status
	const router = server.loopback.Router();

	router.get('/', server.loopback.status());
	server.use(router);
}
