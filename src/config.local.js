/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-04-24 09:13:50
*------------------------------------------------------- */

module.exports = { // eslint-disable-line
	port: process.env.PORT || 3002,
	webUrl: process.env.WEB_URL || 'http: //localhost:3000',
	apiUrl: process.env.API_URL || 'http: //localhost:3005',
};
