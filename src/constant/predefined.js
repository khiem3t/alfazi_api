/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2017-12-02 00:13:30
*------------------------------------------------------- */

export default {
	userStatus: ['active', 'inactive'],
	userLoginType: ['email', 'facebook', 'google'],

	achievementUnlockStatus: ['received', 'notReceived'],

	relationshipStatus: ['pending', 'accepted', 'blocked'],

	trackingType: [
		'questionCreated', 'questionEdited', 'questionDeleted',
		'questionFollowCreated',
		'questionSaveCreated',
		'answerCreated', 'answerEdited', 'answerDeleted',
		'answerLikeCreated',
		'answerPickCreated',
		'reportCreated',
		'relationshipCreated', 'relationshipChangeStatus',
		'reachedAchievement', 'receivedAchievement',
		'receivedBadge',
	],

	notificationType: [
		'questionCreated',
		'answerCreated',
		'replyCreated',
		'answerLikeCreated',
		'answerPickCreated',
		'reportCreated',
		'relationshipCreated', 'relationshipAccepted',
		'upLevel',
		'userCreated',
		'reachedAchievement', 'receivedAchievement',
		'receivedBadge',
	],

	transactionCoinType: [
		'answerPickCreated',
		'answerPickRefund',
		'receivedAchievement',
		'changeQuestionTypeToCoin',
		'questionCreated',
	],

	transactionPointType: [
		'answerCreated',
		'answerLikeCreated',
		'answerPickCreated',
		'questionCreated',
	],

	achievementType: [
		'userCreated',
		'attendance',
		'referral',
		'profileCompleted',
		'reached20Level',
		'reached50Level',
		'reached70Level',
		'reached90Level',
		'reachedTop1Day',
		'reachedTop2Day',
		'reachedTop3Day',
		'reachedTop1Month',
		'reachedTop2Month',
		'reachedTop3Month',
		'reachedTop1Week',
		'reachedTop2Week',
		'reachedTop3Week',
	],

	badgeType: [
		'profileCompleted',
		'reached20Level',
		'reached50Level',
		'reached70Level',
		'reached90Level',
		'reached100Coins',
		'reachedTop1Day',
		'reachedTop1Month',
		'reachedTop1Week',
	],

	reportType: [
		'user',
		'answer',
		'question',
		'reply',
	],

	questionType: [
		'free',
		'coin',
	],
};
