/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
* Created: 2018-04-24 09:23:51
*------------------------------------------------------- */

module.exports = { // eslint-disable-line
	'emailDs': {
		'name': 'emailDs',
		'connector': 'mail',
		'transports': [
			{
				'type': 'smtp',
				'host': process.env.EMAIL_HOST || 'smtp.gmail.com',
				'secure': true,
				'port': 465,
				'tls': {
					'rejectUnauthorized': false
				},
				'auth': {
					'user': process.env.EMAIL || 'alfaziapplication@gmail.com',
					'pass': process.env.EMAIL_PASSWORD || 'Alfazi12#'
				}
			}
		]
	},
	'storage': {
		'name': 'storage',
		'connector': 'loopback-component-storage',
		'provider': 'amazon',
		'acl': 'public-read',
		'key': process.env.S3_KEY || '9194fWU0Sw6FUoSmF2SC2J9fGjJ1Sf8t+ENXHu27',
		'keyId': process.env.S3_KEY_ID || 'AKIAJL6WV45Z2YDYAB5Q',
		'maxFileSize': 20971520,
		'allowedContentTypes': [
			'image/jpg',
			'image/jpeg',
			'image/png',
			'image/tiff'
		]
	}
};
