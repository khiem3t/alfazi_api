/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-22 11:48:13
*------------------------------------------------------- */

export default function (AchievementUnlock) {
	AchievementUnlock.observe('before save', function (ctx, next) {
		const newData = ctx.instance || ctx.data;
		const oldData = ctx.currentInstance || {};
		const isNewInstance = ctx.isNewInstance;

		const Tracking = AchievementUnlock.app.models.Tracking;
		const Notification = AchievementUnlock.app.models.Notification;
		const Achievement = AchievementUnlock.app.models.Achievement;
		const TransactionCoin = AchievementUnlock.app.models.TransactionCoin;

		if (isNewInstance) {
			Notification.create({
				type: 'reachedAchievement',
				receiverId: newData.userId,
				achievementUnlockId: newData.id,
				achievementId: newData.achievementId,
				data: {
					achievementUnlock: newData.__data,
				},
			});
		}

		if (!isNewInstance && oldData.id) {
			if (newData.status === 'received') {
				Achievement.findById(oldData.achievementId, {}, (err, response) => {
					if (err) {
						console.log('err', err);
						return next();
					}

					Tracking.create({
						type: 'receivedAchievement',
						creatorId: ctx.options.accessToken.userId,
						achievementId: oldData.achievementId,
						achievementUnlockId: oldData.id,
						oldData,
						newData,
					});

					Notification.create({
						type: 'receivedAchievement',
						receiverId: oldData.userId,
						achievementUnlockId: oldData.id,
						achievementId: oldData.achievementId,
						data: {
							achievementUnlock: oldData.__data,
							achievement: response.__data,
						},
					});

					TransactionCoin.create({
						type: 'receivedAchievement',
						creatorId: oldData.userId,
						achievementUnlockId: oldData.id,
						data: {
							achievementUnlock: oldData.__data,
							achievement: response.__data,
						},
						amount: response.amount || 0,
					});
				});
			}
		}

		next();
	});
}
