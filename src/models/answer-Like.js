/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */
import { ANSWER_LIKE_REWARD_EXP } from 'src/constant/parameters';

export default function (AnswerLike) {
	AnswerLike.afterRemote('create', (ctx, answerLike, next) => {
		const Tracking = AnswerLike.app.models.Tracking;
		const Notification = AnswerLike.app.models.Notification;
		const TransactionPoint = AnswerLike.app.models.TransactionPoint;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		AnswerLike.findById(answerLike.id, {
			include: [
				{
					relation: 'creator',
					scope: {
						fields: ['username', 'fullName', 'avatar', 'id'],
					},
				},
				{
					relation: 'answer',
					scope: {
						fields: ['id', 'content'],
					},
				},
			],
		}, (err, response) => {
			if (err) {
				console.log('err', err);
				return next();
			}

			const creator = response.creator();
			const answer = response.answer();

			Notification.create({
				type: 'answerLikeCreated',
				creatorId: ctx.args.options.accessToken.userId,
				receiverId: answerLike.receiverId,
				answerId: answerLike.answerId,
				questionId: answerLike.questionId,
				answerLikeId: answerLike.id,
				data: {
					creator: creator.__data,
					answer: answer.__data,
				},
			});

			Tracking.create({
				type: 'answerLikeCreated',
				creatorId: ctx.args.options.accessToken.userId,
				answerId: answerLike.answerId,
				questionId: answerLike.questionId,
				receiverId: answerLike.receiverId,
				answerLikeId: answerLike.id,
				oldData: {},
				newData: answerLike,
			});

			// cộng 5 điểm khi đc like
			TransactionPoint.create({
				type: 'answerLikeCreated',
				creatorId: answerLike.receiverId,
				answerId: answerLike.answerId,
				questionId: answerLike.questionId,
				likeId: answerLike.id,
				data: answerLike,
				amount: ANSWER_LIKE_REWARD_EXP,
			});

			next();
		});
	});
}
