/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */
import { ANSWER_PICK_REWARD_COIN, ANSWER_PICK_REWARD_EXP, QUESTION_FEE_REFUND } from 'src/constant/parameters';

export default function (AnswerPick) {
	AnswerPick.validatesUniquenessOf('questionId');

	AnswerPick.afterRemote('create', (ctx, answerPick, next) => {
		const Tracking = AnswerPick.app.models.Tracking;
		const Notification = AnswerPick.app.models.Notification;
		const TransactionPoint = AnswerPick.app.models.TransactionPoint;
		const TransactionCoin = AnswerPick.app.models.TransactionCoin;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		AnswerPick.findById(answerPick.id, {
			include: [
				{
					relation: 'creator',
					scope: {
						// fields: ['username', 'fullName', 'avatar', 'id', 'coinsCount'],
					},
				},
				{
					relation: 'answer',
					scope: {
						fields: ['id', 'content'],
					},
				},
				{
					relation: 'question',
					scope: {
						fields: ['id', 'type'],
					},
				},
			],
		}, (err, response) => {
			const creator = response.creator();
			const answer = response.answer();
			const question = response.question();

			if (err) {
				console.log('err', err);
				return next();
			}

			Notification.create({
				type: 'answerPickCreated',
				creatorId: ctx.args.options.accessToken.userId,
				receiverId: answerPick.receiverId,
				answerId: answerPick.answerId,
				questionId: answerPick.questionId,
				answerPickId: answerPick.id,
				data: {
					creator: creator.__data,
					answer: answer.__data,
					question: question.__data,
				},
			});

			Tracking.create({
				type: 'answerPickCreated',
				creatorId: ctx.args.options.accessToken.userId,
				answerId: answerPick.answerId,
				questionId: answerPick.questionId,
				receiverId: answerPick.receiverId,
				answerPickId: answerPick.id,
				oldData: {},
				newData: answerPick,
			});

			if (question.type === 'coin') {
				TransactionCoin.create({
					type: 'answerPickRefund',
					creatorId: ctx.args.options.accessToken.userId,
					pickId: answerPick.id,
					amount: QUESTION_FEE_REFUND,
				});
			}

			TransactionCoin.create({
				type: 'answerPickCreated',
				creatorId: answerPick.receiverId,
				pickId: answerPick.id,
				data: answerPick,
				amount: ANSWER_PICK_REWARD_COIN,
			});

			TransactionPoint.create({
				type: 'answerPickCreated',
				creatorId: answerPick.receiverId,
				pickId: answerPick.id,
				data: answerPick,
				amount: ANSWER_PICK_REWARD_EXP,
			});

			next();
		});
	});
}
