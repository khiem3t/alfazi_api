/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */
import { ANSWER_CREATE_REWARD_EXP } from 'src/constant/parameters';

export default function (Answer) {
	Answer.observe('before save', function (ctx, next) {
		const newData = ctx.instance || ctx.data;
		const oldData = ctx.currentInstance || {};
		const isNewInstance = ctx.isNewInstance;

		const Tracking = Answer.app.models.Tracking;

		if (oldData.createdAt && (!newData.createdAt || oldData.createdAt.toString() === newData.createdAt.toString())) {
			if (!ctx.options || !ctx.options.accessToken || !ctx.options.accessToken.userId) {
				return next('Login is required');
			}

			if (!isNewInstance && oldData.id) {
				if (newData.status === 'deleted') {
					Tracking.create({
						type: 'answerDeleted',
						creatorId: ctx.options.accessToken.userId,
						answerId: oldData.id,
						questionId: oldData.questionId,
						oldData,
						newData: {},
					});
				} else {
					Tracking.create({
						type: 'answerEdited',
						creatorId: ctx.options.accessToken.userId,
						answerId: oldData.id,
						questionId: oldData.questionId,
						oldData,
						newData: { ...oldData.__data, ...newData },
					});
				}
			}
		}

		next();
	});

	Answer.afterRemote('create', (ctx, answer, next) => {
		const Tracking = Answer.app.models.Tracking;
		const Notification = Answer.app.models.Notification;
		const TransactionPoint = Answer.app.models.TransactionPoint;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		// cộng 5 điểm Trả lời lần đầu tiên cho câu hỏi này
		Answer.count({ creatorId: answer.creatorId, questionId: answer.questionId }, (err, answersCount) => {
			if (err) {
				return next({ ...err });
			}
			if (answersCount === 1) {
				TransactionPoint.create({
					type: 'answerCreated',
					creatorId: answer.creatorId,
					answerId: answer.id,
					data: answer,
					amount: ANSWER_CREATE_REWARD_EXP,
				});
			}
		});

		Answer.findById(answer.id, {
			include: [
				{
					relation: 'creator',
					scope: {
						fields: ['username', 'fullName', 'avatar', 'id'],
					},
				},
				{
					relation: 'question',
					scope: {
						fields: ['id', 'creatorId', 'content', 'updatedAt', 'answersCount'],
					},
				},
			],
		}, (err, response) => {
			if (err) {
				console.log('err', err);
			} else {
				const creator = response.creator();
				const question = response.question();

				question.updateAttributes({
					updatedAt: new Date(),
					answersCount: question.answersCount + 1,
				});

				Notification.create({
					type: 'answerCreated',
					creatorId: ctx.args.options.accessToken.userId,
					receiverId: question.creatorId,
					answerId: answer.id,
					questionId: answer.questionId,
					data: {
						creator: creator.__data,
						question: question.__data,
						answer: answer.__data,
					},
				});
			}

			Tracking.create({
				type: 'answerCreated',
				creatorId: ctx.args.options.accessToken.userId,
				answerId: answer.id,
				questionId: answer.questionId,
				oldData: {},
				newData: answer,
			});

			next();
		});
	});
}
