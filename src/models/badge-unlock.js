/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-14 15:40:57
*------------------------------------------------------- */

export default function (BadgeUnlock) {
	BadgeUnlock.observe('after save', function (ctx, next) {
		const badgeUnlock = ctx.instance || {};
		const isNewInstance = ctx.isNewInstance;

		if (isNewInstance) {
			const Tracking = BadgeUnlock.app.models.Tracking;
			const Notification = BadgeUnlock.app.models.Notification;
			const Badge = BadgeUnlock.app.models.Badge;

			Tracking.create({
				type: 'receivedBadge',
				creatorId: badgeUnlock.userId,
				badgeUnlockId: badgeUnlock.id,
				badgeId: badgeUnlock.badgeId,
				oldData: {},
				newData: badgeUnlock.__data,
			});

			Badge.findById(badgeUnlock.badgeId, {}, (err, response) => {
				if (err) {
					console.log('err', err);
					return next();
				}
				Notification.create({
					type: 'receivedBadge',
					receiverId: badgeUnlock.userId,
					badgeUnlockId: badgeUnlock.id,
					badgeId: badgeUnlock.badgeId,
					data: {
						badgeUnlock: badgeUnlock.__data,
						badge: response.__data,
					},
				});
				next();
			});
		} else {
			next();
		}
	});
}
