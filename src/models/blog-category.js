/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-20 14:31:31
*------------------------------------------------------- */

export default function (BlogCategory) {
	BlogCategory.validatesUniquenessOf('key');
	BlogCategory.validatesUniquenessOf('name');
}
