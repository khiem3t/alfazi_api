/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-20 14:31:31
*------------------------------------------------------- */

export default function (BlogPost) {
	BlogPost.validatesUniquenessOf('slug');

	BlogPost.afterRemote('findOne', (ctx, post, next) => {
		if (post) {
			const { viewsCount = 0 } = post;

			post.updateAttributes({
				viewsCount: viewsCount + 1,
			});
		}

		next();
	});
}
