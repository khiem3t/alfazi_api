/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */

export default function (QuestionFollow) {
	QuestionFollow.afterRemote('create', (ctx, questionFollow, next) => {
		const Tracking = QuestionFollow.app.models.Tracking;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		Tracking.create({
			type: 'questionFollowCreated',
			creatorId: ctx.args.options.accessToken.userId,
			questionId: questionFollow.questionId,
			questionFollowId: questionFollow.id,
			oldData: {},
			newData: questionFollow,
		});

		next();
	});
}
