/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */

export default function (QuestionSave) {
	QuestionSave.afterRemote('create', (ctx, questionSave, next) => {
		const Tracking = QuestionSave.app.models.Tracking;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		Tracking.create({
			type: 'questionSaveCreated',
			creatorId: ctx.args.options.accessToken.userId,
			questionId: questionSave.questionId,
			questionSaveId: questionSave.id,
			oldData: {},
			newData: questionSave,
		});

		next();
	});
}
