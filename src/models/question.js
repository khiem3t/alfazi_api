/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */
// import generateSlug from 'src/utils/slug';

import { QUESTION_FEE, QUESTION_CREATE_REWARD_EXP } from 'src/constant/parameters';
// import { error } from 'util';

export default function (Question) {
	// // Create remote Method
	// Question.remoteMethod('findBySlug', {
	// 	http: { path: '/:slug', verb: 'get' },
	// 	accepts: [
	// 		{
	// 			arg: 'slug', type: 'any', description: 'Model slug', required: true,
	// 			http: { source: 'path' }
	// 		},
	// 		{
	// 			arg: 'filter', type: 'object',
	// 			description: 'Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})',
	// 		},
	// 		{ arg: 'options', type: 'object', http: 'optionsFromRequest' },
	// 	],
	// 	returns: { root: true, type: 'object' },
	// 	description: 'Find a model instance by {{slug}} from the data source.',
	// 	accessType: 'READ',
	// });
	// // Call remote Method
	// Question.findBySlug = function (slug, filter, options, cb) {

	// 	// code goes here

	// 	cb(null, { 'response': slug });

	// };

	Question.pinToQuestion = function (id, cb) {
		if (id) {
			Question.findOne({
				where: { pin: true }
			}).then((questions) => {
				if (questions) {

					questions.updateAttributes({ pin: false }, (errr) => {
						if (errr) {
							cb(errr);
						}
						Question.findById(id, {}, ( err, que) => {
							if (err) {
								cb(err);
							}
							que.updateAttributes({ pin: true });
						});

					});
					cb(null, { id: id, oldData: questions.id });

				} else {
					Question.findById(id, {}, (e, quee) => {
						if (e) {
							cb(e);
						}
						quee.updateAttributes({ pin: true });
					});
					cb(null, { id: id });
				}
			}).catch((err) => {
				cb(err);
			});
		} else {
			Question.updateAll({ pin: true }, { pin: false }, () => {
				cb(null, {});
			});
		}

	};

	Question.observe('before save', function (ctx, next) {
		const newData = ctx.instance || ctx.data;
		const oldData = ctx.currentInstance || {};
		const isNewInstance = ctx.isNewInstance;

		const Tracking = Question.app.models.Tracking;
		const TransactionCoin = Question.app.models.TransactionCoin;
		const User = Question.app.models.user;

		if (oldData.createdAt && (!newData.createdAt || oldData.createdAt.toString() === newData.createdAt.toString())) {
			if (!ctx.options || !ctx.options.accessToken || !ctx.options.accessToken.userId) {
				return next(null);
			}

			if (!isNewInstance && oldData.id) {
				if (oldData.type === 'free' && newData.type === 'coin') {
					User.findById(ctx.options.accessToken.userId, {
						fields: ['coinsCount', 'id'],
					}, (err, userRes) => {
						if (err) {
							return next(err);
						}

						if (userRes.coinsCount < QUESTION_FEE) {
							return next('Xu cuả bạn không đủ để chuyển câu hỏi này thành câu hỏi xu!');
						}
						TransactionCoin.create({
							type: 'changeQuestionTypeToCoin',
							creatorId: ctx.options.accessToken.userId,
							questionId: oldData.id,
							data: oldData,
							amount: -1 * QUESTION_FEE,
						});
					});
				}

				if (newData.status === 'deleted') {
					Tracking.create({
						type: 'questionDeleted',
						creatorId: ctx.options.accessToken.userId,
						questionId: oldData.id,
						oldData,
						newData: {},
					});
				} else {
					Tracking.create({
						type: 'questionEdited',
						creatorId: ctx.options.accessToken.userId,
						questionId: oldData.id,
						oldData,
						newData: { ...oldData.__data, ...newData },
					});
				}
			}
		}

		// generateSlug(Question, ctx, {
		// 	separator: '-',
		// 	fields: ['title'],
		// 	slug: 'slug',
		// 	lowercase: true,
		// }, function (err) {
		// 	if (err) {
		// 		return next(err);
		// 	}
		// 	next(null);
		// });
		next(null);
	});

	Question.afterRemote('findById', (ctx, question, next) => {
		if (ctx.args && ctx.args.options && ctx.args.options.accessToken && ctx.args.options.accessToken.userId && ctx.args.options.accessToken.userId.toString() !== question.creatorId.toString()) {
			question.updateAttributes({
				viewsCount: question.viewsCount + 1,
			});
		}
		next();
	});

	Question.afterRemote('create', (ctx, question, next) => {
		const Tracking = Question.app.models.Tracking;
		const TransactionCoin = Question.app.models.TransactionCoin;
		const TransactionPoint = Question.app.models.TransactionPoint;
		const Notification = Question.app.models.Notification;
		const User = Question.app.models.user;
		const AchievementUnlock = Question.app.models.AchievementUnlock;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		if (question.type === 'coin') {
			TransactionCoin.create({
				type: 'questionCreated',
				creatorId: ctx.args.options.accessToken.userId,
				questionId: question.id,
				data: question,
				amount: -1 * QUESTION_FEE,
			});
		}

		TransactionPoint.create({
			type: 'questionCreated',
			creatorId: ctx.args.options.accessToken.userId,
			questionId: question.id,
			data: question,
			amount: QUESTION_CREATE_REWARD_EXP,
		});

		Tracking.create({
			type: 'questionCreated',
			creatorId: ctx.args.options.accessToken.userId,
			questionId: question.id,
			oldData: {},
			newData: question,
		});

		Notification.create({
			type: 'questionCreated',
			creatorId: ctx.args.options.accessToken.userId,
			questionId: question.id,
			data: {
				question: question.__data,
			},
		});

		// đăng câu hỏi đầu tiên sẽ tặng xu cho người giới thiệu
		Question.count({ creatorId: ctx.args.options.accessToken.userId }, (err, questionsCount) => {
			if (err) {
				console.log('err', err);
			}
			if (!err && questionsCount === 1) {
				User.findById(ctx.args.options.accessToken.userId, {
					fields: ['referralId', 'id'],
				}, (e, userRes) => {
					if (err) {
						console.log('err', e);
					} else {
						const { referralId } = userRes;

						if (referralId) {
							AchievementUnlock.count({ userId: referralId, achievementId: 'referral' }, (errr, achievementsCount) => {
								if (errr) {
									console.log('errr', errr);
								}
								if (!err && achievementsCount <= 5) {
									AchievementUnlock.findOrCreate({
										where: {
											achievementId: 'referral',
											userId: referralId,
											referredPersonId: ctx.args.options.accessToken.userId,
										}
									}, {
										achievementId: 'referral',
										userId: referralId,
										referredPersonId: ctx.args.options.accessToken.userId,
									});
								}
							});
						}
					}
				});
			}
		});

		next();
	});

	Question.beforeRemote('create', (ctx, question, next) => {
		const User = Question.app.models.user;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		User.findById(ctx.args.options.accessToken.userId, {
			fields: ['coinsCount', 'pointsCount', 'badge', 'id'],
		}, (err, userRes) => {
			if (err) {
				return next(err);
			}

			const { data: questionData = {} } = ctx.args;

			if (userRes.coinsCount < QUESTION_FEE && questionData.type === 'coin') {
				return next('Xu cuả bạn không đủ để đăng câu hỏi này!');
			}

			next();
		});
	});
}
