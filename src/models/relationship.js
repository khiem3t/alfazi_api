/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */
import moment from 'moment';

export default function (Relationship) {
	Relationship.observe('before save', function (ctx, next) {
		const newData = ctx.instance || ctx.data;
		const oldData = ctx.currentInstance || {};
		const isNewInstance = ctx.isNewInstance;

		const Tracking = Relationship.app.models.Tracking;
		const Notification = Relationship.app.models.Notification;

		if (!ctx.options || !ctx.options.accessToken || !ctx.options.accessToken.userId) {
			return next('Login is required');
		}

		if (!isNewInstance && oldData.id && newData.status && oldData.status !== newData.status) {
			if (newData.status === 'accepted') {
				Relationship.findById(oldData.id, {
					include: [
						{
							relation: 'creator',
							scope: {
								fields: ['username', 'fullName', 'avatar', 'id'],
							},
						},
					],
				}, (err, response) => {
					if (err) {
						console.log('err', err);
					} else {
						const creator = response.creator();

						Notification.create({
							type: 'relationshipAccepted',
							creatorId: ctx.options.accessToken.userId,
							receiverId: oldData.creatorId,
							relationshipId: oldData.id,
							data: {
								creator: creator.__data,
								relationship: { ...oldData.__data, ...newData },
							},
						});
					}
				});
			}

			Tracking.create({
				type: 'relationshipChangeStatus',
				creatorId: ctx.options.accessToken.userId,
				friendId: oldData.friendId,
				relationshipId: oldData.id,
				newStatus: newData.status,
				oldData,
				newData: { ...oldData.__data, ...newData },
			});
		}
		next();
	});

	Relationship.beforeRemote('create', (ctx, relationship, next) => {
		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		const User = Relationship.app.models.user;

		Relationship.count({
			createdAt: { gt: new Date(moment().format('MM DD YYYY') + ' 00:00') },
			or: [
				{ creatorId: ctx.args.options.accessToken.userId },
				{ friendId: ctx.args.options.accessToken.userId },
			],
		}, (err, relationsCount) => {
			if (err) {
				return next({ ...err });
			}
			Relationship.count({
				or: [
					{ creatorId: ctx.args.options.accessToken.userId },
					{ friendId: ctx.args.options.accessToken.userId },
				],
			}, (e, relationsCountToday) => {
				if (e) {
					return next({ ...e });
				}
				User.findById(ctx.args.options.accessToken.userId, {
					fields: ['level', 'id'],
				}, (er, userRes) => {
					if (er) {
						return next({ ...er });
					}
					if (relationsCount + 1 > 10 * userRes.level || relationsCountToday + 1 > 5) {
						return next('Bạn phải đạt cấp độ 1 và số bạn tối đa được kết bạn một ngày là 5 và số bạn tối đa là không quá 10 lần số cấp của bạn');
					}
					next();
				});
			});
		});
	});

	Relationship.afterRemote('create', (ctx, relationship, next) => {
		const Tracking = Relationship.app.models.Tracking;
		const Notification = Relationship.app.models.Notification;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		if (relationship.status === 'pending') {
			Relationship.findById(relationship.id, {
				include: [
					{
						relation: 'creator',
						scope: {
							fields: ['username', 'fullName', 'avatar', 'id'],
						},
					},
				],
			}, (err, response) => {
				if (err) {
					console.log('err', err);
				} else {
					const creator = response.creator();

					Notification.create({
						type: 'relationshipCreated',
						creatorId: ctx.args.options.accessToken.userId,
						receiverId: relationship.friendId,
						relationshipId: relationship.id,
						data: {
							creator: creator.__data,
							relationship: relationship.__data,
						},
					});
				}
			});
		}

		Tracking.create({
			type: 'relationshipCreated',
			creatorId: ctx.args.options.accessToken.userId,
			friendId: relationship.friendId,
			relationshipId: relationship.id,
			oldData: {},
			newData: relationship,
		});

		next();
	});
}
