/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-04-04 21:20:51
*------------------------------------------------------- */

export default function (Reply) {
	Reply.afterRemote('create', (ctx, reply, next) => {
		const Tracking = Reply.app.models.Tracking;
		const Notification = Reply.app.models.Notification;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		Reply.findById(reply.id, {
			include: [
				{
					relation: 'creator',
					scope: {
						fields: ['username', 'fullName', 'avatar', 'id'],
					},
				},
			],
		}, (err, response) => {
			if (err) {
				console.log('err', err);
			} else {
				const creator = response.creator();

				if (reply.receiverId !== ctx.args.options.accessToken.userId) {
					Notification.create({
						type: 'replyCreated',
						creatorId: ctx.args.options.accessToken.userId,
						receiverId: reply.receiverId,
						replyId: reply.id,
						questionId: reply.questionId,
						answerId: reply.answerId,
						data: {
							creator: creator.__data,
							reply: reply.__data,
						},
					});

					Tracking.create({
						type: 'replyCreated',
						creatorId: ctx.args.options.accessToken.userId,
						receiverId: reply.receiverId,
						replyId: reply.id,
						questionId: reply.questionId,
						answerId: reply.answerId,
						oldData: {},
						newData: reply,
					});
				}

				ctx.result = {
					...ctx.result.__data,
					creator: creator.__data,
				};
			}

			next();
		});
	});
}
