/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-27 14:05:28
*------------------------------------------------------- */

export default function (Report) {
	Report.afterRemote('create', (ctx, report, next) => {
		const Tracking = Report.app.models.Tracking;
		const Notification = Report.app.models.Notification;

		if (!ctx.args || !ctx.args.options || !ctx.args.options.accessToken || !ctx.args.options.accessToken.userId) {
			return next('Login is required');
		}

		Report.findById(report.id, {
			include: [
				{
					relation: 'creator',
					scope: {
						fields: ['username', 'fullName', 'avatar', 'id'],
					},
				},
				{
					relation: 'receiver',
					scope: {
						fields: ['username', 'fullName', 'avatar', 'id'],
					},
				},
				{
					relation: 'question',
					scope: {
						fields: ['content', 'id'],
					},
				},
				{
					relation: 'answer',
					scope: {
						fields: ['content', 'id'],
					},
				},
			],
		}, (err, response) => {
			if (err) {
				console.log('err', err);
			} else {
				const creator = response.creator();

				Notification.create({
					type: 'reportCreated',
					creatorId: ctx.args.options.accessToken.userId,
					receiverId: report.receiverId,
					questionId: report.questionId,
					answerId: report.answerId,
					reportId: report.id,
					data: {
						creator: creator.__data,
						report: response.__data,
					},
				});
			}

			Tracking.create({
				type: 'reportCreated',
				creatorId: ctx.args.options.accessToken.userId,
				receiverId: report.receiverId,
				questionId: report.questionId,
				answerId: report.answerId,
				reportId: report.id,
				oldData: {},
				newData: report,
			});

			next();
		});
	});
}
