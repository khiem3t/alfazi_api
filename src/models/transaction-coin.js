/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-14 01:34:47
*------------------------------------------------------- */

export default function (TransactionCoin) {
	TransactionCoin.observe('after save', function (ctx, next) {
		const transCoin = ctx.instance || {};
		const isNewInstance = ctx.isNewInstance;

		if (isNewInstance) {
			const User = TransactionCoin.app.models.user;

			User.findById(transCoin.creatorId, {
				// fields: ['coinsCount', 'pointsCount', 'badge', 'level', 'id'],
			}, (er, userRes) => {
				if (er) {
					return next(er);
				}
				let { coinsCount = 0 } = userRes;

				// cộng 5 điểm khi đc like
				coinsCount += transCoin.amount || 0;

				userRes.updateAttributes({
					coinsCount,
				});

				next();
			});
		} else {
			next();
		}
	});
}
