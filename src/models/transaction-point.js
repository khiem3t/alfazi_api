/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-03-14 01:34:47
*------------------------------------------------------- */

export default function (TransactionPoint) {
	TransactionPoint.observe('after save', function (ctx, next) {
		const transPoint = ctx.instance || {};
		const isNewInstance = ctx.isNewInstance;

		if (isNewInstance) {
			const Notification = TransactionPoint.app.models.Notification;
			const User = TransactionPoint.app.models.user;
			const AchievementUnlock = TransactionPoint.app.models.AchievementUnlock;
			const BadgeUnlock = TransactionPoint.app.models.BadgeUnlock;

			User.findById(transPoint.creatorId, {
				// fields: ['coinsCount', 'pointsCount', 'badge', 'level', 'id'],
			}, (er, userRes) => {
				if (er) {
					return next(er);
				}
				let { pointsCount = 0, level = 0, attendance = { loggedIn: false, oldPoints: 0 } } = userRes;

				// cộng 5 điểm khi đc like
				pointsCount += transPoint.amount || 0;

				// điểm danh tăng xu
				if (attendance && !attendance.loggedIn && pointsCount - attendance.oldPoints >= 10) {
					// AchievementUnlock.findOrCreate({
					// 	where: {
					// 		achievementId: 'attendance',
					// 		userId: transPoint.creatorId,
					// 	}
					// }, {
					// 	achievementId: 'attendance',
					// 	userId: transPoint.creatorId,
					// });

					AchievementUnlock.create({
						achievementId: 'attendance',
						userId: transPoint.creatorId,
					});

					attendance.loggedIn = true;
				}

				// tính level
				const newLevel = Math.floor((Math.sqrt((pointsCount * 20) + 25) - 5) / 10);

				if (newLevel > level) {
					level = newLevel;

					Notification.create({
						type: 'upLevel',
						receiverId: transPoint.creatorId,
						data: {
							newLevel,
						},
					});

					if (newLevel === 20) {
						AchievementUnlock.findOrCreate({
							where: {
								achievementId: 'reached20Level',
								userId: transPoint.creatorId,
							}
						}, {
							achievementId: 'reached20Level',
							userId: transPoint.creatorId,
						});

						BadgeUnlock.findOrCreate({
							where: {
								badgeId: 'reached20Level',
								userId: transPoint.creatorId,
							}
						}, {
							badgeId: 'reached20Level',
							userId: transPoint.creatorId,
						});
					}
					if (newLevel === 50) {
						AchievementUnlock.findOrCreate({
							where: {
								achievementId: 'reached50Level',
								userId: transPoint.creatorId,
							}
						}, {
							achievementId: 'reached50Level',
							userId: transPoint.creatorId,
						});

						BadgeUnlock.findOrCreate({
							where: {
								badgeId: 'reached50Level',
								userId: transPoint.creatorId,
							}
						}, {
							badgeId: 'reached50Level',
							userId: transPoint.creatorId,
						});
					}
					if (newLevel === 70) {
						AchievementUnlock.findOrCreate({
							where: {
								achievementId: 'reached70Level',
								userId: transPoint.creatorId,
							}
						}, {
							achievementId: 'reached70Level',
							userId: transPoint.creatorId,
						});

						BadgeUnlock.findOrCreate({
							where: {
								badgeId: 'reached70Level',
								userId: transPoint.creatorId,
							}
						}, {
							badgeId: 'reached70Level',
							userId: transPoint.creatorId,
						});
					}
					if (newLevel === 90) {
						AchievementUnlock.findOrCreate({
							where: {
								achievementId: 'reached90Level',
								userId: transPoint.creatorId,
							}
						}, {
							achievementId: 'reached90Level',
							userId: transPoint.creatorId,
						});

						BadgeUnlock.findOrCreate({
							where: {
								badgeId: 'reached90Level',
								userId: transPoint.creatorId,
							}
						}, {
							badgeId: 'reached90Level',
							userId: transPoint.creatorId,
						});
					}
					if (newLevel === 100) {
						AchievementUnlock.findOrCreate({
							where: {
								achievementId: 'reached100Level',
								userId: transPoint.creatorId,
							}
						}, {
							achievementId: 'reached100Level',
							userId: transPoint.creatorId,
						});

						BadgeUnlock.findOrCreate({
							where: {
								badgeId: 'reached100Level',
								userId: transPoint.creatorId,
							}
						}, {
							badgeId: 'reached100Level',
							userId: transPoint.creatorId,
						});
					}
				}

				userRes.updateAttributes({
					pointsCount,
					level,
					attendance,
				});

				next();
			});
		} else {
			next();
		}
	});
}
