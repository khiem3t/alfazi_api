/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-02-12 00:54:41
*------------------------------------------------------- */

import login from 'src/utils/login';
import loginFb from 'src/utils/loginFb';
import loginGg from 'src/utils/loginGg';
import sendMailVerify from 'src/utils/sendMailVerify';
import loopback from 'loopback';

import predefined from 'src/constant/predefined';

export default function (User) {
	User.validatesInclusionOf('loginType', { in: predefined.userLoginType });
	User.validatesInclusionOf('status', { in: predefined.userStatus });
	User.validatesUniquenessOf('phone');


	// User.disableRemoteMethod('create', true);
	User.disableRemoteMethod('deleteById', true);
	User.disableRemoteMethod('__delete__accessTokens', true);
	User.disableRemoteMethod('__create__accessTokens', true);
	User.disableRemoteMethod('__destroyById__accessTokens', true);

	User.beforeRemote('create', (ctx, u, next) => {
		const { data: user = {} } = ctx.args;

		if (!user.email && !user.phone) {
			return next({
				statusCode: 400,
				code: 'EMAIL_PHONE_REQUIRED',
				message: '{{email}} is required'
			});
		}
		next();
	});

	User.afterRemote('create', (ctx, user, next) => {
		// tạo tài khoản được tặng 20 xu
		const AchievementUnlock = User.app.models.AchievementUnlock;

		AchievementUnlock.findOrCreate({
			where: {
				achievementId: 'userCreated',
				userId: user.id,
			}
		}, {
			achievementId: 'userCreated',
			userId: user.id,
		});

		sendMailVerify(User, user, next);
	});

	User.login = login;

	User.loginFacebook = loginFb;

	User.loginGoogle = loginGg;


	// send password reset link when requested
	User.on('resetPasswordRequest', (info) => {
		const webUrl = User.app.get('webUrl');

		const params = { resetLink: webUrl + '/reset-password?access_token=' + info.accessToken.id };

		const renderer = loopback.template('src/email/reset-password.ejs');
		let html = renderer(params);

		User.app.models.Email.send({
			to: info.email,
			from: `"${process.env.EMAIL_NAME || 'Acctualize Web Services'}" <${process.env.EMAIL || 'acctualize.info@gmail.com'}>`,
			subject: '[Alfazi] Đặt lại mật khẩu.',
			html: html
		}, (err) => {
			if (err) {
				return console.log('> error sending password reset email', err);
			}
			console.log('> sending password reset email to:', info.email);
		});
	});

	User.beforeRemote('resetPassword', (ctx, opt, next) => {
		if (ctx.args.options.email) {
			User.findOne({ where: { email: ctx.args.options.email }, fields: { status: true, loginType: true } }, function (err, user) {
				if (err) {
					return next(err);
				}
				if (user && user.status === 'inactive') {
					return next({
						code: 'ACCOUNT_DISABLED',
						message: 'Account has been disabled',
						name: 'Error',
						status: 401,
						statusCode: 401
					});
				}
				if (user && user.loginType !== 'email') {
					return next({
						code: 'ACCOUNT_INVALID',
						message: 'Bạn đã đăng nhập email này thông qua ' + user.loginType,
						name: 'Error',
						status: 401,
						statusCode: 401
					});
				}
				return next();
			});
		} else {
			next();
		}
	});

	User.observe('after save', function (ctx, next) {
		const userData = ctx.instance || {};
		const isNewInstance = ctx.isNewInstance;

		const AchievementUnlock = User.app.models.AchievementUnlock;
		const BadgeUnlock = User.app.models.BadgeUnlock;

		// if (!ctx.options || !ctx.options.accessToken || !ctx.options.accessToken.userId) {
		// 	return next('Bạn chưa đăng nhập');
		// }

		if (
			!isNewInstance
			&& userData.cover // eslint-disable-line
			&& userData.avatar // eslint-disable-line
			&& userData.fullName // eslint-disable-line
			&& userData.gender // eslint-disable-line
			&& userData.birthDate // eslint-disable-line
			&& userData.phone // eslint-disable-line
			&& userData.email // eslint-disable-line
			&& userData.address // eslint-disable-line
			&& userData.desc // eslint-disable-line
			&& userData.school // eslint-disable-line
			&& userData.class // eslint-disable-line
			&& userData.province // eslint-disable-line
		) {
			AchievementUnlock.findOrCreate({
				where: {
					achievementId: 'profileCompleted',
					userId: userData.id,
				}
			}, {
				achievementId: 'profileCompleted',
				userId: userData.id,
			});

			BadgeUnlock.findOrCreate({
				where: {
					badgeId: 'profileCompleted',
					userId: userData.id,
				}
			}, {
				badgeId: 'profileCompleted',
				userId: userData.id,
			});
		}
		next();
	});
}

