/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-05-13 08:57:54
*------------------------------------------------------- */
import { FB } from 'fb';
import { PASSWORD_DEFAULT } from 'src/constant/parameters';

import sendMailVerify from 'src/utils/sendMailVerify';

export default function (accessToken, referralId, ttl, include = '{}', next) {
	const self = this;

	FB.api('me', { fields: 'email,name,gender,picture,link', 'access_token': accessToken }, function (res) {
		if (res.error) {
			return next({ ...res.error });
		}

		if (!res.id) {
			return next({ message: 'accessToken is invalid!' });
		}

		if (!res.email) {
			return next({ message: 'Email not found!' });
		}

		// user.password = PASSWORD_DEFAULT;

		const userData = {
			email: res.email,
			fullName: res.name || '',
			gender: res.gender || '',
			facebookId: res.id,
			loginType: 'facebook',
			avatar: res.picture && res.picture.data && res.picture.data.url,
			password: PASSWORD_DEFAULT,
			referralId: referralId,
		};

		self.findOne({ where: { email: userData.email } }, (err, userCheck) => {
			if (err) {
				return next({ ...err });
			}

			if (!userCheck) {
				self.create(userData, (errCreate, userCreate) => {
					if (errCreate) {
						return next({ ...errCreate });
					}

					sendMailVerify(self, userCreate);

					// tạo tài khoản được tặng 20 xu
					const AchievementUnlock = self.app.models.AchievementUnlock;

					AchievementUnlock.findOrCreate({
						where: {
							achievementId: 'userCreated',
							userId: userCreate.id,
						}
					}, {
						achievementId: 'userCreated',
						userId: userCreate.id,
					});

					self.login({
						email: userCreate.email,
						password: PASSWORD_DEFAULT,
						ttl
					}, include, function (errLogin, token) {
						if (errLogin) {
							return next({ ...errLogin });
						}

						return next(null, token);
					});
				});
			} else {
				if (userCheck.loginType === 'facebook') {
					self.login({
						email: userCheck.email,
						password: PASSWORD_DEFAULT,
						ttl
					}, include, function (errLogin, token) {
						if (errLogin) {
							return next({ ...errLogin });
						}

						return next(null, token);
					});
				} else {
					return next({ message: 'Email already exists!' });
				}
			}
		});
	});
}
