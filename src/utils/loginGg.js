/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-05-13 08:57:54
*------------------------------------------------------- */
import GoogleAuth from 'google-auth-library';

import sendMailVerify from 'src/utils/sendMailVerify';

import { PASSWORD_DEFAULT } from 'src/constant/parameters';

const auth = new GoogleAuth();
const client = new auth.OAuth2([process.env.GG_CUSTOMER_ID], process.env.GG_CLIENT_SECRET_CODE, '');

export default function (accessToken, referralId, ttl, include = '{}', next) {
	const self = this;

	client.verifyIdToken(accessToken, [process.env.GG_CUSTOMER_ID], function (e, res) {
		if (e) {
			return next(e);
		}

		const payload = res.getPayload();


		if (payload.error) {
			return next({ ...payload.error });
		}

		if (!payload.sub) {
			return next({ message: 'accessToken is invalid!' });
		}

		if (!payload.email) {
			return next({ message: 'Email not found!' });
		}

		// user.password = PASSWORD_DEFAULT;

		const userData = {
			email: payload.email,
			fullName: payload.name || '',
			googleId: payload.sub,
			loginType: 'google',
			avatar: payload.picture || '',
			password: PASSWORD_DEFAULT,
			referralId: referralId,
		};

		self.findOne({ where: { email: userData.email } }, (err, userCheck) => {
			if (err) {
				return next({ ...err });
			}

			if (!userCheck) {
				self.create(userData, (errCreate, userCreate) => {
					if (errCreate) {
						return next({ ...errCreate });
					}

					sendMailVerify(self, userCreate);

					// tạo tài khoản được tặng 20 xu
					const AchievementUnlock = self.app.models.AchievementUnlock;

					AchievementUnlock.findOrCreate({
						where: {
							achievementId: 'userCreated',
							userId: userCreate.id,
						}
					}, {
						achievementId: 'userCreated',
						userId: userCreate.id,
					});

					self.login({
						email: userCreate.email,
						password: PASSWORD_DEFAULT,
						ttl
					}, include, function (errLogin, token) {
						if (errLogin) {
							return next({ ...errLogin });
						}

						return next(null, token);
					});
				});
			} else {
				if (userCheck.loginType === 'google') {
					self.login({
						email: userCheck.email,
						password: PASSWORD_DEFAULT,
						ttl
					}, include, function (errLogin, token) {
						if (errLogin) {
							return next({ ...errLogin });
						}

						return next(null, token);
					});
				} else {
					return next({ message: 'Email already exists!' });
				}
			}
		});
	});
}
