/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com
*
*
* Created: 2018-05-13 09:19:46
*------------------------------------------------------- */

export default (Model, user, next) => {
	if (!user.email) {
		return next();
	}

	const webUrl = Model.app.get('webUrl');
	const apiUrl = Model.app.get('apiUrl');

	const [protocol, host] = apiUrl.split('://');

	const options = {
		type: 'email',
		host,
		protocol,
		port: protocol === 'https' ? 443 : 80,
		to: user.email,
		from: `"${process.env.EMAIL_NAME || 'Alfazi Web Services'}" <${process.env.EMAIL || 'alfaziapplication@gmail.com'}>`,
		subject: '[Alfazi] Chúc mừng bạn đã đăng ký tài khoản thành công.',
		template: 'src/email/verify.ejs',
		redirect: webUrl + '/email-verified',
		user: user,
		webUrl,
	};

	user.verify(options, (err, response) => {
		if (err) {
			console.log('verification email sent err', err);
			// User.deleteById(user.id);
			// return next(err);
		}

		console.log('> verification email sent:', response);

		if (typeof next === 'function') {
			next();
		}
	});
};
